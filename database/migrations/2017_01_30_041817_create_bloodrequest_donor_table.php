<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloodrequestDonorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blood_request_donor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('blood_request_id');
            $table->integer('donor_id');
            $table->date('donated_at')->nullable();
            $table->string('status')->default('pending');            
            $table->timestamps();
            $table->unique(['blood_request_id','donor_id']);
            $table->index(['blood_request_id', 'donor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blood_request_donor');
    }
}
