<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Donor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'national_id' => $faker->numberBetween(11111111,9999999),
        'avater' => $faker->imageUrl($width = 200, $height = 200),
        'state_id' => $faker->numberBetween(1,7),
        'city' => 'Seri Kembangan',
        'address' => $faker->address,
        'email' => $faker->safeEmail,
        'blood_group_id' => $faker->numberBetween(1,7),
    ];
});
