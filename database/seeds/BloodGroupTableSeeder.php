<?php

use Illuminate\Database\Seeder;

class BloodGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blood_groups')->insert([
            'name' => "A+",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "B+",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "A-",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "B-",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "AB+",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "AB-",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "O+",
        ]);
        DB::table('blood_groups')->insert([
            'name' => "O-",
        ]);
    }
}
