<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'name' => "Mahchan Goalhi",
        ]);
        DB::table('states')->insert([
            'name' => "Maafannu",
        ]);
        DB::table('states')->insert([
            'name' => "Henveyru",
        ]);
        DB::table('states')->insert([
            'name' => "Galolhu",
        ]);
        DB::table('states')->insert([
            'name' => "Villigili",
        ]);
        DB::table('states')->insert([
            'name' => "Hulhumale",
        ]);
    }
}
