# Blood donation System for Hospitals
In order to setup the system run the following commands.

 * `composer install`
 * `cp .env.example .env`
 * `php artisan migrate:refresh --seed`

**Update the database credentials before running the migration**