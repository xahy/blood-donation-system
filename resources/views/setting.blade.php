@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
	    <div class="col-md-8">
	    	<h2>{{ auth()->user()->name }}</h2>
	    	<hr>
	    	<p class="bg-info" style="padding: 5px 8px;">Department</p>
	    	<p> {{ auth()->user()->department }}</p>
	    	<br>
	    	<p class="bg-info" style="padding: 5px 8px;"> Email</p>
	    	<p>{{ auth()->user()->email }}</p>
	    </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Change Password</div>
                <div class="panel-body">
					<form action="/setting/updatepass" method="POST">
						{{ csrf_field() }}
						<label for="">Current Password</label>
						<input type="password" name="curr_pass" class="form-control" />
						<hr>
						<label for="">New Password</label>
						<input type="password" name="new_pass" class="form-control" />
						<br>
						<label for="">Confirm Password</label>
						<input type="password" name="confirm_pass" class="form-control" />
						<br>
						<input type="submit" class="btn btn-primary" value="Update">
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
