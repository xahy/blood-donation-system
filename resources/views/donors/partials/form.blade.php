<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Full Name</label>

    <div class="col-md-6">
        {!! Form::text('name', old('name'), ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('blood_group_id') ? ' has-error' : '' }}">
    <label for="blood_group_id" class="col-md-4 control-label">Blood Group</label>

    <div class="col-md-6">  
        {!! Form::select('blood_group_id', $blood_groups, old('blood_group_id') ,['class' => 'form-control']) !!}      
        @if ($errors->has('blood_group_id'))
        <span class="help-block">
            <strong>{{ $errors->first('blood_group_id') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Email</label>

    <div class="col-md-6">        
        {!! Form::email('email',old('email'), ['class' => 'form-control']) !!}
        @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
    <label for="phone_number" class="col-md-4 control-label">Mobile Number</label>

    <div class="col-md-6">        
        {!! Form::text('phone_number', old('phone_number'), ['class'=>'form-control']) !!}
        @if ($errors->has('phone_number'))
        <span class="help-block">
            <strong>{{ $errors->first('phone_number') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('national_id') ? ' has-error' : '' }}">
    <label for="national_id" class="col-md-4 control-label">National Id</label>

    <div class="col-md-6">        
        {!! Form::text('national_id', old('national_id'), ['class'=>'form-control']) !!}
        @if ($errors->has('national_id'))
        <span class="help-block">
            <strong>{{ $errors->first('national_id') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label for="address" class="col-md-4 control-label">Address</label>

    <div class="col-md-6">        
        {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}
        @if ($errors->has('address'))
        <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    <label for="city" class="col-md-4 control-label">City</label>

    <div class="col-md-6">        
        {!! Form::text('city', old('city'), ['class' => 'form-control']) !!}
        @if ($errors->has('city'))
        <span class="help-block">
            <strong>{{ $errors->first('city') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
    <label for="state_id" class="col-md-4 control-label">State</label>

    <div class="col-md-6">
        {!! Form::select('state_id', $states, old('state_id') ,['class' => 'form-control']) !!}         
        @if ($errors->has('state_id'))
        <span class="help-block">
            <strong>{{ $errors->first('state_id') }}</strong>
        </span>
        @endif
    </div>
</div>




<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
             Submit
        </button>
    </div>
</div>
