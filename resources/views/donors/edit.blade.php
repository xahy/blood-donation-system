@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Donor</div>

                <div class="panel-body">

                    {!! Form::model($donor,['url' => 'donors/'.$donor->id, 'method' => 'put','class'=>'form-horizontal'])  !!}                 
                        @include('donors.partials.form')                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection