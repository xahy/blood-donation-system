@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Donor Information</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <style>
                                .blood_group {
                                    background:#d05b5b;
                                    width:150px;
                                    height:150px;
                                    color:#fff;
                                    display: flex;
                                    font-size: 40px;
                                    margin:0px auto;
                                    justify-content: center;
                                    align-items: center;
                                    border-radius: 50%;
                                }
                                .donor-info tr > td {
                                    padding-bottom: 10px;
                                }
                                .donor-info tr > td:first-child {
                                    color:#8c8c8c;
                                    padding-right: 15px;
                                }
                            </style>
                            <div class="blood_group">{{ $donor->blood_group->name }}</div>
                        </div>
                        <div class="col-md-9">
                            <h2>{{ $donor->name }}</h2>
                            <hr>
                            <table class="donor-info">
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $donor->email }}</td>
                                </tr>
                                <tr>
                                    <td>National ID</td>
                                    <td>{{ $donor->national_id }}</td>
                                </tr>
                                <tr>
                                    <td>Contact number</td>
                                    <td>{{ $donor->phone_number }}</td>
                                </tr>
                                <tr>
                                    <td>State</td>
                                    <td>{{ $donor->state->name }}</td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>{{ $donor->city }}</td>
                                </tr>

                                <tr>
                                    <td>Address</td>
                                    <td>{{ $donor->address }}</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

            </div>
            
            @if ($donor->blood_requests()->latest())                
            <div class="panel panel-default">
                <div class="panel-heading">Donor Activities</div>
                <table class="table">
                    <tr>
                        <th>Hospital</th>
                        <th>Donated</th>
                        <th>Actions</th>
                    </tr>
                    @foreach ($donor->blood_requests as $blood_request)                        
                    <tr>
                        <td>{{ $blood_request->hospital->name }}</td>
                        <td>{{ $blood_request->pivot->status }}</td>
                        <td>
                            @if ($blood_request->pivot->status != 'donated')
                                <a class="btn btn-sm btn-primary" href="{{ url('requests/'.$blood_request->id) }}">View Request</a>
                            @endif                            
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection