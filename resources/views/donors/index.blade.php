@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Search Donor
                </div>
                <div class="panel-body">
                    <form action="/donors">
                        <label for="">National ID: </label>
                        <input type="text" class="form-control" name="id" value="{{ $national_id }}">
                        <br>
                        <input type="submit" class="btn btn-info" value="Search">
                        <a href="{{ url('/donors') }}" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   All Donors
                   <div class="pull-right">
                      <a href="{{ url('donors/create') }}" class="">Add New</a>                      
                   </div>
                </div>

                <table class="table">
                    <tr>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Contact </th>
                        <th> Blood Group </th>
                        <th> Options </th>
                    </tr>
                    @foreach($donors as $donor)
                    <tr>
                        <td> {{ $donor->name }} </td>
                        <td> {{ $donor->email }} </td>
                        <td> {{ $donor->phone_number }} </td>
                        <td> {{ $donor->blood_group->name }} </td>
                        <td>
                            <a class="btn btn-sm btn-default" href="{{ url('donors/'.$donor->id) }}">Show Details</a>
                            <a class="btn btn-sm btn-info" href="{{ url('donors/'.$donor->id.'/edit') }}">Edit</a>
                            <a class="btn btn-sm btn-danger" href="{{ url('donors/'.$donor->id.'/destroy') }}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </table>                
            </div>
            {{ $donors->links() }}
        </div>
    </div>
</div>
@endsection
