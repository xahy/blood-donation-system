@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Donor</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('donors') }}">
                        {{ csrf_field() }}
                        @include('donors.partials.form')
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection