@if(session()->has('msg'))
	<script>
		$.notify({
			message: '{{ session('msg') }}' 
		},{
			type: '{{ session('msg-type') }}',
			animate: {
				enter: 'animated fadeInDown',
				exit: 'animated fadeOutUp'
			},
			placement: {
				from: "top",
				align: "center"
			}
		});
	</script>
@endif