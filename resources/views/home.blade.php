@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Pending Requests
                    <div class="pull-right">
                      <a href="{{ url('requests/create') }}" class="">Add New</a>                      
                   </div>
                </div>
                    <table class="table">
                        <tr>
                            <th>Blood Group</th>
                            <th>Created at</th>
                            <th>Created by</th>
                            <th>Accepted Donors</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($requests as $request)
                            <tr>
                                <td>{{ $request->blood_group->name }}</td>
                                <td>{{ $request->created_at->diffForHumans() }}</td>
                                <td>{{ $request->user->name }}</td>
                                <td>{{ count($request->donors) }}</td>
                                <td>
                                    <a class="btn btn-sm btn-info" href="{{ url('requests/'.$request->id.'/edit') }}">Edit</a>
                                    <a class="btn btn-sm btn-danger" href="{{ url('requests/'.$request->id.'/destroy') }}">Delete</a>
                                    <a class="btn btn-sm btn-primary" href="{{ url('requests/'.$request->id) }}">View Donors</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
