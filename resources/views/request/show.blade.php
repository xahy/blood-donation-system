@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Request information #{{ $bloodrequest->id }}
                </div>
                <div class="panel-body">
                    <p>Group: {{ $bloodrequest->blood_group->name }}</p>
                    <p>Description: {{ $bloodrequest->description }}</p>
                </div>
                    <table class="table">
                        <tr>
                            <th> Name </th>
                            <th> Contact </th>
                            <th> Status </th>
                            <th> Options </th>
                        </tr>
                       @foreach($bloodrequest->donors as $donor)
                        <tr class="{{ $donor->isSelected() ? "success" : "" }}">
                            <td> {{ $donor->name }} </td>
                            <td> {{ $donor->phone_number }} </td>
                            <td> {{ $donor->statusInfo() }} </td>
                            <td>
                            @if ($donor->isSelected())
                                <a class="btn btn-sm btn-danger" href="{{ url('requests/'.$bloodrequest->id.'/deallocate/'.$donor->id) }}">De-Select Donor</a>   
                                <a class="btn btn-sm btn-success" href="{{ url('requests/'.$bloodrequest->id.'/donated/'.$donor->id) }}">Successfully donated Blood</a>   
                            @else
                                <a class="btn btn-sm btn-info" href="{{ url('requests/'.$bloodrequest->id.'/allocate/'.$donor->id) }}">Select Donor</a>
                            @endif
                            <a href="#" class="btn btn-sm btn-default js-donor-details" data-email="{{ $donor->email }}" data-name="{{ $donor->name }}" data-phone_number="{{ $donor->phone_number }}">Contact Details</a>
                            </td>
                        </tr>
                        @endforeach

                    </table>
            </div>
        </div>
    </div>
</div>
@endsection


@section('footer')
<div class="modal fade" tabindex="-1" role="dialog" id="donor-contact-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" >Contact: <span id="js-modal-name"></span></h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td>Email</td>
						<td id="js-modal-email"></td>
						<td></td>
					</tr>
					<tr>
						<td>Contact Number</td>
						<td id="js-modal-phone_number"></td>
						<td></td>
					</tr>
					<tr>
						<td>Email</td>
						<td id="js-modal-email"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script>
	$('.js-donor-details').on('click',function(){
		var btn = $(this);
		var name = btn.data('name');
		var email = btn.data('email');
		var phone_number = btn.data('phone_number');

		var pre_selct = 'js-modal-';

		$('#'+pre_selct+'name').html(name);
		$('#'+pre_selct+'email').html(email);
		$('#'+pre_selct+'phone_number').html(phone_number);
		$('#donor-contact-modal').modal();
	});
</script>
@endsection