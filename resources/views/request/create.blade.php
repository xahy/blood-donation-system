@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Request</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('requests') }}">
                        {{ csrf_field() }}
                        @include('request.partials.form')
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection