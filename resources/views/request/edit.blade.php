@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Request</div>

                <div class="panel-body">

                    {!! Form::model($bloodrequest,['url' => 'requests/'.$bloodrequest->id, 'method' => 'put','class'=>'form-horizontal'])  !!}                 
                        @include('request.partials.form')                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection