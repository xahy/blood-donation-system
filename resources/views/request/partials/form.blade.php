
<div class="form-group{{ $errors->has('blood_group_id') ? ' has-error' : '' }}">
    <label for="blood_group_id" class="col-md-4 control-label">Select Blood Group</label>

    <div class="col-md-6">  
        {!! Form::select('blood_group_id', $blood_groups, old('blood_group_id') ,['class' => 'form-control']) !!}      
        @if ($errors->has('blood_group_id'))
        <span class="help-block">
            <strong>{{ $errors->first('blood_group_id') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description" class="col-md-4 control-label">Description (More info)</label>

    <div class="col-md-6">        
        {!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
        @if ($errors->has('description'))
        <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
        @endif
    </div>
</div>

<input type="hidden" name="hospital_id" value="{{ auth()->user()->hospital }}">
<input type="hidden" name="user_id" value="1">

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
             Submit
        </button>
        <a href="{{ url('requests') }}" class="btn btn-default">Cancel</a>
    </div>
</div>
