<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Hospital Name</label>

    <div class="col-md-6">
        {!! Form::text('name', old('name'), ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label for="address" class="col-md-4 control-label">Address</label>

    <div class="col-md-6">
        {!! Form::textarea('address', old('address'), ['class'=>'form-control']) !!}
        @if ($errors->has('address'))
        <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
    <label for="state_id" class="col-md-4 control-label">State</label>

    <div class="col-md-6">
        {!! Form::select('state_id', $states, old('state_id') ,['class' => 'form-control']) !!}         
        @if ($errors->has('state_id'))
        <span class="help-block">
            <strong>{{ $errors->first('state_id') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    <label for="city" class="col-md-4 control-label">City</label>

    <div class="col-md-6">
        {!! Form::text('city', old('city'), ['class'=>'form-control']) !!}
        @if ($errors->has('city'))
        <span class="help-block">
            <strong>{{ $errors->first('city') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('lat') || $errors->has('long') ? ' has-error' : '' }}">
    <label for="lat" class="col-md-4 control-label">GeoLocation of the Hospital</label>

    <div class="col-md-6">
    <style>#map {width:100%;height: 300px;margin-bottom: 10px;}</style>
        <div id="map"></div>
        {!! Form::text('lat', old('lat'), ['class'=>'form-control','id'=>'js-lat']) !!}
        <br>
        {!! Form::text('long', old('long'), ['class'=>'form-control','id'=>'js-long']) !!}
        @if ($errors->has('lat') || $errors->has('lat'))
        <span class="help-block">
            <strong>{{ $errors->first('lat') }} {{ $errors->first('long') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
    <label for="logo" class="col-md-4 control-label">Logo (Url of the logo image)</label>

    <div class="col-md-6">
        {!! Form::text('logo', old('logo'), ['class'=>'form-control']) !!}
        @if ($errors->has('logo'))
        <span class="help-block">
            <strong>{{ $errors->first('logo') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
             Submit
        </button>
    </div>
</div>

@section('scripts')
<script>
function markerDragged(event) {
	document.getElementById('js-lat').value = event.latLng.lat();
    document.getElementById('js-long').value = event.latLng.lng();
}
function initMap() {
	console.log("raw value",document.getElementById('js-lat').value);
	console.log("Parseint value",parseFloat(document.getElementById('js-lat').value));

	if(!document.getElementById('js-lat').value) {

		var deflat = 4.176148;
		var deflong = 73.509369;
	} else {
		var deflat = parseFloat(document.getElementById('js-lat').value);
		var deflong = parseFloat(document.getElementById('js-long').value);
	}
	var myLatLng = {lat: deflat, lng: deflong};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: myLatLng
	});
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		draggable:true
	});

	marker.addListener('drag', markerDragged);
	marker.addListener('dragend', markerDragged);
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB05hKeeIzSw_62q9c7bJLbG5vUvjTnUO4&callback=initMap">
</script>
@stop
