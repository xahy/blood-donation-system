@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Hospital</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('hospitals') }}">
                        {{ csrf_field() }}
                        @include('hospitals.partials.form')
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection