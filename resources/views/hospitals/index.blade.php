@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   All Hospitals
                   <div class="pull-right">
                      <a href="{{ url('hospitals/create') }}" class="">Add New</a>                      
                   </div>
                </div>

                <table class="table">
                    <tr>
                        <th></th>
                        <th> Name </th>
                        <th> City </th>
                        <th> Options </th>
                    </tr>
                    @foreach($hospitals as $hospital)
                    <tr>
                        <td> <img src="{{ $hospital->logo }}" style="width:60px;" alt=""> </td>
                        <td> {{ $hospital->name }} </td>
                        <td> {{ $hospital->city }} </td>
                        <td>
                            <a class="btn btn-sm btn-info" href="{{ url('hospitals/'.$hospital->id.'/edit') }}">Edit</a>
                            <a class="btn btn-sm btn-danger" href="{{ url('hospitals/'.$hospital->id.'/destroy') }}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
