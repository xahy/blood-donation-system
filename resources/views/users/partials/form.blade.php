<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Full Name</label>

    <div class="col-md-6">
        {!! Form::text('name', old('name'), ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Email</label>

    <div class="col-md-6">
        {!! Form::email('email', old('email'), ['class'=>'form-control']) !!}
        @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
    <label for="department" class="col-md-4 control-label">Department name</label>

    <div class="col-md-6">
        {!! Form::text('department', old('department'), ['class'=>'form-control']) !!}
        @if ($errors->has('department'))
        <span class="help-block">
            <strong>{{ $errors->first('department') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group">
	<label for="" class="col-md-4 control-label">Roles</label>
	<div class="col-md-6">
		@foreach ($roles as $role)
		<div class="checkbox">
		    <label>
		        	{!! Form::checkbox("roles[]", $role->id ,$user->hasRole($role->id)) !!}
		        	{{ $role->name }}
			</label>
		</div>
	    @endforeach
	</div>
</div>
@php
    if(auth()->user()->isAdminAndSupervisor()) {
@endphp
<div class="form-group">
    <label for="" class="col-md-4 control-label">Hospital</label>
    <div class="col-md-6">
        {!! Form::select('hospital_id', $hospitals, old('hospital_id') ,['class' => 'form-control']) !!}      
        @if ($errors->has('hospital_id'))
        <span class="help-block">
            <strong>{{ $errors->first('hospital_id') }}</strong>
        </span>
        @endif
    </div>
</div>
@php
    }
@endphp

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
             Submit
        </button>
    </div>
</div>