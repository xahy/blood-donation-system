@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit User</div>

                <div class="panel-body">

                    {!! Form::model($user,['url' => 'users/'.$user->id, 'method' => 'put','class'=>'form-horizontal'])  !!}                 
                        @include('users.partials.form')                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection