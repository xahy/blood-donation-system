@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   All Users
                   <div class="pull-right">
                      <a href="{{ url('users/create') }}" class="">Add New</a>                      
                   </div>
                </div>

                <table class="table">
                    <tr>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Hospital </th>
                        <th> User Roles</th>
                        <th> Options </th>
                    </tr>
                    @foreach($users as $user)
                    <tr>
                        <td> {{ $user->name }} </td>
                        <td> {{ $user->email }} </td>
                        <td>
                            @if (!$user->hasRole(1))
                                 {{ $user->hospital->name }} 
                            @endif                             
                        </td>
                        <td>
                            @foreach ($user->roles as $role)
                                <span class="label label-warning">{{ $role->name }}</span>
                            @endforeach
                        </td>
                        <td>
                            <a class="btn btn-sm btn-info" href="{{ url('users/'.$user->id.'/edit') }}">Edit</a>
                            <a class="btn btn-sm btn-danger" href="{{ url('users/'.$user->id.'/destroy') }}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
