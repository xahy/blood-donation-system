<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'department', 'hospital_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
        return $this->belongsTomany(Role::class);
    }

    public function hasRole($role) {
        return $this->roles->pluck('id')->contains($role);
    }

    public function hospital() {
        return $this->belongsTo(Hospital::class);
    }

    public function isAdmin() {
        return $this->hasRole('1');
    }

    public function isAdminAndSupervisor() {
        return $this->hasRole('1') || $this->hasRole('2') ? true : false;
    }

    public function isNotAdmin() {
        return $this->hasRole('2') || $this->hasRole('3') ? true : false;
    }

    public function getLogo() {
        if ($this->hospital) {
            return $this->hospital->logo ? $this->hospital->logo : asset('images/logo.png');
        } 
        return asset('images/logo.png');
    }
}
