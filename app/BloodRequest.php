<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class BloodRequest extends Model
{
	protected $fillable = ['description','blood_group_id','user_id','hospital_id','confirmed'];
	// protected $fillable = ['description','blood_group_id','confirm']; 
    //

    public function scopePending($query)
    {
        return $query->where('confirmed', '=', 0);
    }

    public function blood_group() {
    	return $this->belongsTo('App\BloodGroup');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function donors() {
        return $this->belongsToMany('App\Donor')->withTimestamps()->withPivot(['status']);
    }

    public function hospital() {
        return $this->belongsTo('App\Hospital');
    }

    // public function isSelected() {
    //     if (self::donors()->pivot->status == 'selected') {
    //         return true;
    //     }
    //     return false;
    // }

}
