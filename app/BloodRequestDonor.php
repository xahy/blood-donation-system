<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BloodRequestDonor extends Model
{
	protected $table = "blood_request_donor";
    protected $fillable = ["blood_request_id","donor_id","status"];

    public function donor() {
    	return $this->belongsTo('App\Donor');
    }

    public function blood_request() {
    	return $this->belongsTo('App\BloodRequest');
    }

}
