<?php 
namespace App\DonorRequest;

use DB;
use App\BloodRequest;
use App\BloodRequestDonor;
use App\Donor;
use Carbon\Carbon;

class DonorRequestRepository {

	public $donor_id;
	public $bloodrequest_id;


	public function __construct() {

	}

	public function allocateDonor($bloodrequest_id, $donor_id) {
		$this->donor_id = $donor_id;
		$this->bloodrequest_id = $bloodrequest_id;		
		$this->deallocateAllDonors();
		$pivot = DB::table('blood_request_donor')->select('id')->where('blood_request_id',$bloodrequest_id)->where('donor_id',$donor_id)->update(['status'=>'selected']);
	}

	public function  deallocateDonor($bloodrequest_id, $donor_id) {
		$this->donor_id = $donor_id;
		$this->bloodrequest_id = $bloodrequest_id;
		$pivot = DB::table('blood_request_donor')->select('id')->where('blood_request_id',$bloodrequest_id)->where('donor_id',$donor_id)->update(['status'=>'accepted']);
	}	

	public function deallocateAllDonors() {
		DB::table('blood_request_donor')->select('id')->where('blood_request_id',$this->bloodrequest_id)->where('status','selected')->update(['status'=>'accepted']);
	}

	public static function acceptRequest($bloodRequest, $donor) {
		$message = [];
		$re = BloodRequestDonor::where('donor_id',$donor->id)->where('status','!=',"donated")->first();
 		if($re) {
 			$message['status'] = 400;
 			$message['message'] = "You have already accepted request";
 			return $message;
 		}

 		if ($donor->blood_group->id != $bloodRequest->blood_group->id) {
 			$message['status'] = 400;
 			$message['message'] = "The blood group does not match";
 			return $message;
 		}

 		$lastDonatedDate = Donor::getLastDonatedDate($donor->id);
 		$now = Carbon::now();
 		if($lastDonatedDate) {
 			$donatedDate = Carbon::parse($lastDonatedDate);
 			if ($donatedDate->diffInDays($now) < 80) {
 				$message['status'] = 400;
	 			$message['message'] = "It has being ".$donatedDate->diffInDays($now)." days since you donated blood. You need to wait at least 80 days.";
	 			return $message;
 			}
 		}

		BloodRequestDonor::create(["blood_request_id"=>$bloodRequest->id,"donor_id"=>$donor->id,"status"=>'accepted']);
		$message['message'] = "Success, wait for the approval for the request";
		return $message;
	
	}

	public static function donatedBlood($donor_id, $bloodrequest_id) {		
		DB::table('blood_requests')->select('id')->where('id',$bloodrequest_id)->update(['confirmed'=>1]);
		$pivot = DB::table('blood_request_donor')->select('id')->where('blood_request_id',$bloodrequest_id)->where('donor_id',$donor_id)->update(['status'=>'donated','donated_at'=>Carbon::now()]);
		DB::table('blood_request_donor')->select('id')->where('blood_request_id',$bloodrequest_id)->where('donor_id',$donor_id)->where('status','!=','donated')->delete();
	}

}


