<?php 

namespace App\DonorRequest;

class RequestStatus {


	public static function GetStatusInfo($status) {
		$statusInfo = [
			'pending' 	=> 	'New request has been sent to the donors and waiting for a response',
			'accepted' 	=>	'Donor has accepted the request and waiting for a confirmation from the system to be selected',
			'selected' 	=> 	'The donor is selected and confirmed from the system',
			'success'	=>	'The donor has arrived and has donated blood'
		];
		return $statusInfo[$status];
	}

	public function isPending($status) {
		if($status == 'pending') {
			return true;
		}
		return false;
	}

	public function isAccepted($status) {
		if($status == 'accepted') {
			return true;
		}
		return false;
	}

	public function isSelected($status) {
		if($status == 'selected') {
			return true;
		}
		return false;
	}

	public function isDonatedBlood($status) {
		if($status == 'success') {
			return true;
		}
		return false;
	}



}