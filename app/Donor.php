<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\DonorRequest\RequestStatus;

class Donor extends Model
{
    protected $fillable = ['username','email','national_id','name','avater','phone_number','address','state_id','city','blood_group_id','auth_token','one_signal'];

    protected $hidden = array('password');

    public function blood_group() {
    	return $this->hasOne('App\BloodGroup','id','blood_group_id');
    }

    public function state() {
    	return $this->hasOne('App\State', 'id', 'state_id');
    }



    public function blood_requests() {
    	return $this->belongsToMany('App\BloodRequest')->withTimestamps()->withPivot(['status','donated_at']);

        // $donor->blood_requests()->updateExistingPivot($donor_request_id, ['status'=>'published']);
    }

    public function blood_request_donor() {
        return $this->hasMany('App\BloodRequestDonor','donor_id','id');
    }

    public static function getLastDonatedDate($donor_id) {
        $req = static::find($donor_id)->blood_request_donor()->orderBy('donated_at', 'desc')->first();
        if($req) {
            return $req->donated_at;
        }
        return null;
    }

    public function isSelected() {
        return $this->pivot->status == 'selected' ? true : false;
    }

    public function statusInfo() {
        return RequestStatus::GetStatusInfo($this->pivot->status);
    }

    public function lastDonated() {
        
    }


}
