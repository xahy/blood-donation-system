<?php

namespace App\Http\Middleware;

use Closure;
use App\Donor;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $header = $request->header('Authorization');
        if (! $header) {
            abort(403);
        }
        $donor = Donor::where('auth_token','=',$header)->with('blood_group')->first();

        if(! $donor) {
            abort(403);
        }

        $request->session()->put('donor', $donor);

        return $next($request);
    }
}
