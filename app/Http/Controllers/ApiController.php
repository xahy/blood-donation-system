<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Donor;
use App\BloodRequest;
use App\DonorRequest\DonorRequestRepository;
use Illuminate\Support\Facades\Hash;
use App\BloodRequestDonor;
use Carbon\Carbon;


class ApiController extends Controller
{
    protected $donor;
    function __construct(Request $request)
    {
        $this->donor = $request->session()->get('donor',null);
    }

    public function getDonor() {
        return $this->donor;
    }
    public function login(Request $request) {
    	$email = $request->input('email');
    	$password = $request->input('password');
        $one_signal = $request->input('one_signal');

    	$donor = Donor::where('email','=',$email)->first();
        if (! $donor) {
            abort(403);
        }
        if (! Hash::check($password, $donor->password)) {
            abort(403);
        }
        $this->removeOneSignal($request->input('one_signal'));
        $donor->auth_token = str_random(20);
        $donor->one_signal = $one_signal;
        $donor->save();


    	return $donor;
    }

    public function register(Request $request) {
        $validation = Validator::make($request->all(),[ 
            'national_id' => 'required|unique:donors|max:255',
            'email' => 'required|email|unique:donors',
            'password' => 'required|max:255',
            'name' => 'required|max:255',
            'phone_number' => 'required|numeric',
            'state_id' => 'required|exists:states,id',
            'city' => 'required',
            'blood_group_id' => 'required|exists:blood_groups,id'
        ]);
        if ($validation->fails()) {
            return response($validation->errors(), 400)
                  ->header('Content-Type', 'application/json');
            // return response(400)->json($validation->errors());
        }

        $this->removeOneSignal($request->input('one_signal'));
        $donor = Donor::create($request->all());
        $donor->auth_token = str_random(20);        
        $one_signal = $request->input('one_signal');
        $donor->one_signal = $one_signal;
        $donor->password = Hash::make($request->input('password'));
        $donor->save();
        return $donor;


    }

    public function changePassword(Request $request) {
        $old_password = $request->input('old_password');
        $password = $request->input('password');
        $confirm_password = $request->input('confirm_password');

        if (! Hash::check($old_password, $this->donor->password)) {
            $error['message'] = "Make sure to enter the correct current password";
            return response($error, 400)
                  ->header('Content-Type', 'application/json');
        }


        if ($password != $confirm_password) {
            $error['message'] = "Make sure to enter the same password in the confirm password";
            return response($error, 400)
                  ->header('Content-Type', 'application/json');
        }

        $this->donor->password = Hash::make($password);
        $this->donor->save();
        $error['message'] = "Password has been successfuly changed";
        return response($error, 200)
                  ->header('Content-Type', 'application/json');

    }

    private function removeOneSignal($one_signal) {
        $donors = Donor::where('one_signal',$one_signal)->get();
        foreach ($donors as $donor) {
            $donor->one_signal = null;
            $donor->save();
        }        
    }

    public function profile() {
        $profile['donor'] = $this->donor;
        $profile['history'] = $this->donor->blood_request_donor()->with('blood_request.hospital')->where('status','donated')->orderBy('donated_at', 'desc')->get();
        $d = $profile['history']->first();
        if ($d) {
            $now = Carbon::now();
            $lastdonatedDate = Carbon::parse($d->donated_at);
            $profile['dayslastdonate'] = $lastdonatedDate->diffInDays($now);
        } else {
            $profile['dayslastdonate'] = 0;
        }
        
        
        return response($profile, 200)
                  ->header('Content-Type', 'application/json');
    }

    public function getRequests() {
        // $request = $this->donor->getRequests();
        $blood_group_id = $this->donor->blood_group->id;
    	$request = BloodRequest::where('blood_group_id','=',$blood_group_id)->where('confirmed','=',0)->with('blood_group','hospital')->get();
    	return $request;
    }

    public function getActiveRequests() {
        $donor = $this->donor;
        $donorRequest = BloodRequestDonor::where('donor_id',$donor->id)->where('status','!=','donated')->with('blood_request.hospital')->get();
        return $donorRequest;
    }


    public function acceptRequest(Request $request) {
        // $donorid = get from the access token
        $requestid = $request->input('blood_request_id');
        $bloodRequest = BloodRequest::findOrFail($requestid);

        $data = DonorRequestRepository::acceptRequest($bloodRequest,$this->donor);
        if (isset($data['status'])) {
            return response($data, 400)
                  ->header('Content-Type', 'application/json');
        }
        return response($data, 200)
                  ->header('Content-Type', 'application/json');
    }


}
