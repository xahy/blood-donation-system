<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Role;
use App\Hospital;
use App\Http\Requests;

class UserController extends Controller
{


    public function __construct() {
        $this->middleware(['auth','roles:adminAndSupervisor']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $hospitals = Hospital::all()->pluck('name','id');;
        $user = new User;
        return view('users.create',compact('roles','user','hospitals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create($request->all());
        $user->password = bcrypt("123");
        $user->update();

        if(!isset($request->roles)){
            $user->roles()->attach(3);
        } else {
            $user->roles()->attach($request->roles);
        }
        

        return redirect('users')->with([
            'msg'=>'A new user has been created and saved',
            'msg-type' => 'success'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user = User::findOrFail($id);
        $hospitals = Hospital::all()->pluck('name','id');
        return view('users.edit',compact('user','roles','hospitals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);        
        $user->update($request->all());

        $user->roles()->sync($request->roles);

        return redirect('users')->with([
            'msg'=>'Users detail has been updated',
            'msg-type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
