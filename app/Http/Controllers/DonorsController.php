<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Donor;
use App\Http\Requests;
use App\BloodGroup;
use App\State;
use App\BloodRequest;

class DonorsController extends Controller
{
    public function __construct() {
    	$this->middleware('auth',['except'=>['makeRequest']]);
    }

    public function index(Request $request) {
        $national_id = "";
        if($request->input('id')) {
            $national_id = $request->input('id');
            $donors = Donor::with('blood_group')->where('national_id','=',$request->input('id'))->latest()->paginate(15);
        } else {
            $donors = Donor::with('blood_group')->latest()->paginate(15);
        }    	
    	return view('donors.index',compact('donors','national_id'));
    }

    public function show($id) {
        $donor = Donor::findOrFail($id);        
        return view('donors.show', compact('donor'));
    }

    public function create() {
        $blood_groups = BloodGroup::all()->pluck('name','id');
        $states = State::all()->pluck('name','id');
    	return view('donors.create', compact('blood_groups','states'));
    }

    public function store(Requests\DonorRequest $request) {
    	$donor = Donor::create($request->all());        
        return redirect('donors')->with([
            'msg'=>'A new Donor has been created and saved',
            'msg-type' => 'success'
        ]);
    }

    public function edit($id) {
    	$donor = Donor::findOrFail($id);
        $blood_groups = BloodGroup::all()->pluck('name','id');
        $states = State::all()->pluck('name','id');
    	return view('donors.edit',compact('donor','blood_groups','states'));
    }

    public function update($id, Requests\DonorRequest $request) {
        $donor = Donor::findOrFail($id);
    	$donor->update($request->all());
        return redirect('donors')->with([
            'msg'=>'Donors information has been updated',
            'msg-type' => 'success'
        ]);
    }


    public function destroy($id) {
    	//delete
    }

    public function makeRequest(Donor $donor,BloodRequest $blood_request) {
        $donor->blood_requests()->attach($blood_request);
        
        return $donor->blood_requests;
    }

}
