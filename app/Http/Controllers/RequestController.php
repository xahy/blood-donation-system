<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\BloodRequest;
use App\BloodGroup;
use App\Donor;
use DB;
use App\DonorRequest\DonorRequestRepository;
use Carbon\Carbon;

class RequestController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {           
        if(!auth()->user()->hospital) {
            $requests = BloodRequest::pending()->get();
        } else {
            $requests = BloodRequest::pending()->where('hospital_id','=',auth()->user()->hospital->id)->get();
        }
        
        return view('request.index',compact('requests'));
    }


    public function create()
    {
        $blood_groups = BloodGroup::all()->pluck('name','id');
        return view('request.create',compact('blood_groups'));
    }


    public function store(Request $request)
    {        
        $request->merge(['hospital_id'=>auth()->user()->hospital->id]);
        $request->merge(['user_id'=>auth()->user()->id]);
        $bloodRequest = BloodRequest::create($request->all());
        $num_donors = $this->notifyDonors($bloodRequest);
        return redirect('requests')->with([
            'msg'=>'A new request has been created and being notified to '.$num_donors.' donors',
            'msg-type' => 'success'
        ]);
    }


    public function show($id)
    {
        $bloodrequest = BloodRequest::with(['donors'=>function($query){
            $query->orderBy('status', 'desc');
        }])->find($id);
        return view('request.show',compact('bloodrequest'));
    }


    public function edit($id)
    {

        $bloodrequest = BloodRequest::findorFail($id);
        if ($bloodrequest->hospital_id == auth()->user()->hospital->id) {
            $blood_groups = BloodGroup::all()->pluck('name','id');
            return view('request.edit',compact('bloodrequest','blood_groups'));
        }
        return abort(403, "You are not allowed to edit this");
    }


    public function update(Request $request, $id)
    {
        $bloodrequest = BloodRequest::findorFail($id);
        $request->merge(['hospital_id'=>auth()->user()->hospital->id]);
        $request->merge(['user_id'=>auth()->user()->id]);
        $bloodrequest->update($request->all());
        return redirect('requests')->with([
            'msg'=>'Blood request information has been updated',
            'msg-type' => 'success'
        ]);
    }


    public function destroy($id)
    {
        //
    }


    public function allocateDonor($blood_request_id, $donor_id, DonorRequestRepository $repository) {
        $donor = Donor::find($donor_id);
        $repository->allocateDonor($blood_request_id, $donor_id);
        if ($donor->one_signal != null) {
            $userids[] = $donor->one_signal;
            $message = "You have been approved from the hospital. Please come to the hospital to donate blood. Thank you";
            $this->sendOneSignal($message,$userids);
        }
        return redirect('requests/'.$blood_request_id)->with([
            'msg'=>'The donor has being selected for this request',
            'msg-type' => 'success'
        ]);
    }

    public function deallocateDonor($blood_request_id, $donor_id, DonorRequestRepository $repository) {
        $repository->deallocateDonor($blood_request_id, $donor_id);
        return redirect('requests/'.$blood_request_id)->with([
            'msg'=>'The donor has being de-selected from this request',
            'msg-type' => 'success'
        ]);
    }

    public function donatedDonor($blood_request_id, $donor_id, DonorRequestRepository $repository) {
        $repository->donatedBlood($donor_id,$blood_request_id);
        return redirect('requests')->with([
            'msg'=>'The request has been closed and archived',
            'msg-type' => 'success'
        ]);
    }


    public function notifyDonors($bloodRequest) {
        $userids = [];
    	//	1. fetch the donors based on the blood Group 
        $now = Carbon::now();
        $donors = Donor::where('blood_group_id',$bloodRequest->blood_group_id)->where('one_signal','!=','null')->get();
        foreach ($donors as $donor) {
            $lastDonatedDate = Donor::getLastDonatedDate($donor->id);            
            if($lastDonatedDate) {
                $donatedDate = Carbon::parse($lastDonatedDate);
                if ($donatedDate->diffInDays($now) > 80) {
                   $userids[] = $donor->one_signal;
                }
            } else {
                $userids[] = $donor->one_signal;
            }
        }

        if (count($userids) > 0 ) {
            $message = $bloodRequest->blood_group->name . " blood is needed at ". $bloodRequest->hospital->name;
            $this->sendOneSignal($message,$userids);
            return count($userids);
        }

        return 0;
    	
    }


    function sendOneSignal($message,$user_ids) {
        $content = [
            "en" => $message,
        ];

        $fields = [
            'app_id' => "3944f4dc-e5b3-4367-b1b1-3acaa31d1820",
            'include_player_ids' => $user_ids, //The player id
            'isIos' => true,
            'data' => [
                'blood_group' =>'A+',
            ],
            'contents' => $content,
            'headings' => array('en'=>'Blood Request'),
        ];

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Basic MDQwNzBjOGItNmJmNi00ODQ5LWEwMGYtZDQwMjg4NGYwMDA2'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
    }

}
