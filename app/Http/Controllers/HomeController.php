<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\BloodRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!auth()->user()->hospital) {
            $requests = BloodRequest::pending()->get();
        } else {
            $requests = BloodRequest::pending()->where('hospital_id','=',auth()->user()->hospital->id)->get();
        }
        
        return view('request.index',compact('requests'));
    }
}
