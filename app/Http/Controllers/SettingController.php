<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    
    public function index() {
    	return view('setting');
    }

    public function updatepass(Request $request) {
    	$curr_pass = $request->input('curr_pass');
    	$new_pass = $request->input('new_pass');
    	$confirm_pass = $request->input('confirm_pass');

    	if (Hash::check($curr_pass, auth()->user()->password)) {
    		if($new_pass === $confirm_pass) {

    			auth()->user()->password = bcrypt($new_pass);
    			auth()->user()->update();

    			return redirect('setting')->with([
		            'msg'=>'Password is successfully changed',
		            'msg-type' => 'success'
		        ]);
    		}
    		return redirect('setting')->with([
	            'msg'=>'The new password and confirm password does not match',
	            'msg-type' => 'danger'
	        ]);
    	} 
    	return redirect('setting')->with([
            'msg'=>'The old password does not match with the current password',
            'msg-type' => 'danger'
        ]);
    }
}
