<?php

Route::get('/', 'HomeController@index');
Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/setting', 'SettingController@index');
Route::post('/setting/updatepass', 'SettingController@updatepass');
Route::resource('/donors', 'DonorsController');
Route::resource('/requests', 'RequestController');
Route::resource('/hospitals', 'HospitalController');
Route::resource('/users', 'UserController');


Route::get('/donors/{donor}/request/{blood_request}', 'DonorsController@makeRequest');
Route::get('/requests/{blood_request_id}/allocate/{donor_id}', 'RequestController@allocateDonor');
Route::get('/requests/{blood_request_id}/deallocate/{donor_id}', 'RequestController@deallocateDonor');
Route::get('/requests/{blood_request_id}/donated/{donor_id}', 'RequestController@donatedDonor');


Route::group(['prefix' => 'api/v1'], function () {
    Route::post('/login', 'ApiController@login');
    Route::post('/register', 'ApiController@register');
    Route::get('/profile', 'ApiController@profile')->middleware('api.auth');
    Route::post('/changePassword', 'ApiController@changePassword')->middleware('api.auth');
    Route::get('/requests', 'ApiController@getRequests')->middleware('api.auth');
    Route::get('/requests/active', 'ApiController@getActiveRequests')->middleware('api.auth');
    Route::post('/request/accept','ApiController@acceptRequest')->middleware('api.auth');
});



