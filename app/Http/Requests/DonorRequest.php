<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DonorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'national_id' => 'required',
            'name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'state_id' => 'required',
            'city' => 'required',
            'blood_group_id' => 'required'
        ];
    }
}
